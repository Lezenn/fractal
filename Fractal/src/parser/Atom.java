package parser;

import core.TokenType;

public class Atom extends Node {

	public Atom(String content) {
		super(TokenType.ATOM, content, null);
	}

	@Override
	public boolean isList() {
		return false;
	}

	@Override
	public boolean eval() {
		return false;
	}
}
