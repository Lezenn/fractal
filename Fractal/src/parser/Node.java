package parser;

import java.util.List;

import core.TokenType;

public abstract class Node {
	
	protected TokenType type;
	protected String content;
	protected List<Node> nodes;
	
	public Node(TokenType t, String content, List<Node> nodes) {
		this.type = t;
		this.content = content;
		this.nodes = nodes;
	}
	
	public abstract boolean isList();
	
	public abstract boolean eval();
	
	public TokenType getType() {
		return type;
	}

	public String getContent() {
		return content;
	}

	public List<Node> getNodes() {
		return nodes;
	}
}
