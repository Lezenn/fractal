package parser;

import java.util.List;

import core.TokenType;

public class Define extends Node {
	
	private Atom functionName;
	
	public Define(List<Node> nodes) throws SyntaxException {
		super(TokenType.DEFINE, "define", nodes.subList(1, nodes.size()));
		if (!(nodes.get(0) instanceof Atom)) {
			throw new SyntaxException();
		} else {
			this.functionName = (Atom) nodes.get(0);
		}
	}

	@Override
	public boolean isList() {
		return false;
	}

	@Override
	public boolean eval() {
		return false;
	}

	public Atom getFunctionName() {
		return functionName;
	}
	
}
