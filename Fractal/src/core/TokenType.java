package core;

public enum TokenType {
	
	LPAREN("\\("),
	RPAREN("\\)"),
	LBRACE("\\["),
	RBRACE("\\]"),
	DEFINE("define"),
	ATOM("[a-zA-Z]+[a-zA-Z0-9]*"),
	DOUBLE("-?[0-9]+\\.[0-9]+"),
	ADD("\\+"),
	SUB("\\-"),
	MUL("\\*"),
	DIV("\\/"),
	REM("\\%"),
	CHAR("\\'.\\'"),
	STRING("\"[^\\\"\\\\]*(\\\\.[^\\\"\\\\]*)*\""),
	INTEGER("-?[0-9]+"),
	WHITESPACE("[ \t\r\n]");
	
	private String regex;
	
	private TokenType(String r) {
		this.regex = r;
	}
	
	public String getRegex() {
		return this.regex;
	}
	
}
