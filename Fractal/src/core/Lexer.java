package core;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
	
	private String sourceCode;
	private List<Token> tokens;
	
	public Lexer(String source) {
		this.sourceCode = source;
		this.tokens = new ArrayList<Token>();
	}
	
	public void lex() {
		while (sourceCode.length() > 0) {
			String match = new String();
			boolean found = false;
			for (TokenType type : TokenType.values()) {
				String regex = type.getRegex();
				Pattern p = Pattern.compile("^" + regex);
				Matcher m = p.matcher(sourceCode);
				if (m.find()) {
					match = m.group();
					sourceCode = sourceCode.substring(match.length());
					found = true;
					if (type != TokenType.WHITESPACE) {
						tokens.add(new Token(type, match));
					}
				}
			}
			if (!found) {
				System.err.println("Cannot lex \n" + sourceCode);
				System.exit(1);
			}
		}
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public List<Token> getTokens() {
		return tokens;
	}
	
}
