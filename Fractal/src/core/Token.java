package core;

public class Token {
	
	private TokenType type;
	private String content;
	
	public Token(TokenType t, String s) {
		this.type = t;
		this.content = s;
	}

	public TokenType getType() {
		return type;
	}
	
	public String getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "Token [type=" + type + "\t" + content + "]";
	}
	
}
