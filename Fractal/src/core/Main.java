package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	
	public static String readFile(String path) {
		StringBuilder b = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line = br.readLine();
			while(line != null) {
				b.append(line).append("\n");
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return b.toString();
	}
	
	public static void main(String...args) {
		String code = readFile("code/source_code.frac");
		Lexer lexer = new Lexer(code);
		lexer.lex();
	}
	
}
